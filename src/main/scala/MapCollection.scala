object MapCollection:


  /***
   * TODO: Sum all values of map m
   *       Example: m = Map("A" -> 1, "B" -> 2, "C" -> 10) ; returns 13
   */
  def sumValues(m: Map[String, Int]): Int = ???

  /***
   * TODO: concatenate all the keys of map m
   *       Example: m1 = Map("A" -> 1, "B" -> 2, "C" -> 10) ; returns "ABC"
   */
  def concatKeys(m: Map[String, Int]): String = ???

  
  /***
   * TODO: Add a String s as a key of map m. If s does not exist, add with value i, if it exists, sum i to the value.
   *       Example: s = "B", i=4, m = Map("A" -> 3, "B" -> 1, "C" ->2) ; returns Map("A" -> 3, "B" -> 5, "C" ->2)
   */
  def addToMap(s: String, i: Int, m: Map[String, Int]): Map[String, Int] = ??? 

  
  /***
   * TODO: transform the list of String into a map which keys are each unique String in m, and which values are the number of occurrences of the key.
   *       use the previous addToMap function
   *       Example: l = List("A", "B", "A", "C", "C", "A") ; returns Map("A" -> 3, "B" -> 1, "C" ->2)
   */
  def toMap(l: List[String]): Map[String, Int] = ???
    

  /***
   * TODO: Create a fusion of maps m1 and m2, by adding the values of equal keys
   *       use the previous addToMap function
   *       Example: m1 = Map("A" -> 1, "B" -> 2) ; m2 = Map("A" -> 5, "C" -> 10) ; returns Map( "A" -> 6, "B" -> 2, "C" -> 10)
   */
  def fusion(m1: Map[String, Int], m2: Map[String, Int]): Map[String, Int] = ???


  /***
   * TODO: Transform the map m into a Map which keys are the values of m and which values are the list of
   *       m keys. Create at least two scalatest tests.
   *       Example: m = Map("A" -> 1, "B" -> 2, "C" -> 1) ; returns Map( 1 -> List("A", "C"), 2 -> List("B"))
   */
  def invert[A,B](m: Map[A, B]): Map[B, List[A]] = ???
