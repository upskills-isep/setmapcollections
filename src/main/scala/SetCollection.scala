object SetCollection:
  /***
   * TODO: flatten the Set sl, without using the flatten function. Create at least two scalatest tests.
   *       Example: s = Set(List("A", "B"), List("C", "D") ; returns Set("A", "B", "C", "D")
   */
  def flatten[A](sl: Set[List[A]]): Set[A] = ???

  /***
   * TODO: calculate the intersection of all the sets in ss. Create at least two scalatest tests.
   *       Example: s = Set(List("A", "B", "C"), List("C", "D") ; returns Set("C")
   */
  def intersection[A](ss: Set[Set[A]]): Set[A] = ???

  /***
   * TODO: calculate the union of all the sets in ss. Create at least two scalatest tests.
   *       Example: s = Set(List("A", "B"), List("C", "D") ; returns Set("A", "B", "C", "D")
   */
  def union[A](ss: Set[Set[A]]): Set[A] = ???

  /***
   * TODO: expand the Set s to contain as many tuples as the combination of set s with set si. Create at least two scalatest tests.
   *       Example: s = Set("A", "B") ; si = Set(2, 3) ; returns Set( ("A", 2), ("A", 3) , ("B", 2), ("B", 3))
   */
  def expand[A,B](ss: Set[A], si: Set[B]): Set[(A, B)] = ???

